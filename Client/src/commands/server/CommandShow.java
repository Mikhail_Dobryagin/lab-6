package commands.server;

import com.company.CommandProcessingServerReturn;
import spacemarine.SpaceMarine;
import util.ErrorReturn;

import java.util.LinkedList;


public class CommandShow implements CommandProcessingServerReturn
{

    public static void longLine()
    {
        System.out.println("________________________________________________________________________________________________________________________");
    }

    /**
     *
     * @param argsFromServer -- ErrorReturn, SpaceMarine * k (0 <= k <= n)
     */
    @Override
    public void execute(LinkedList<Object> argsFromServer)
    {
        ErrorReturn errorReturn = (ErrorReturn)argsFromServer.poll();

        if(errorReturn.getCode()!=0)
        {
            System.out.println(errorReturn.getStatus());
            return;
        }

        System.out.println();
        longLine();

        while(!argsFromServer.isEmpty())
            System.out.println(((SpaceMarine)argsFromServer.poll()).show());

        longLine();
        System.out.println();
    }
}
