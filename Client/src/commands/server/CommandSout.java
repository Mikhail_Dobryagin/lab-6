package commands.server;

import com.company.CommandProcessingServerReturn;

import java.util.LinkedList;


public class CommandSout implements CommandProcessingServerReturn
{
    @Override
    public void execute(LinkedList<Object> argsFromServer)
    {
        System.out.println(argsFromServer.poll());
    }
}
