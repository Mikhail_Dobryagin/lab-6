package commands.client;

import com.company.Command;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandFilterStartsWithName implements Command
{
    @Override
    public ErrorReturn execute(LinkedList<Object> outputArgs)
    {
        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, LinkedList<Object> outputArgs)
    {
        outputArgs.add(args[0]);
        return ErrorReturn.OK();
    }
}
