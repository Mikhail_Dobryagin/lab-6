package commands.client;

import com.company.Command;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandRemoveById implements Command
{
    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, LinkedList<Object> outputArgs)
    {
        try {
            outputArgs.add(Long.parseLong(((String)args[0])));
            return ErrorReturn.OK();
        }catch (NumberFormatException e)
        {
            return new ErrorReturn(2, "Ошибка при вводе Id");
        }
    }

    @Override
    public ErrorReturn execute(LinkedList<Object> outputArgs)
    {
        return null;
    }
}
