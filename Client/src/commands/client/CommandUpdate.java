package commands.client;

import com.company.Command;
import spacemarine.Chapter;
import util.EndOfScanException;
import util.ErrorReturn;
import spacemarine.SpaceMarine;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class CommandUpdate implements Command
{
    @Override
    public ErrorReturn execute(LinkedList<Object> outputArgs)
    {
        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, LinkedList<Object> outputArgs)
    {
        SpaceMarine newSm;

        if(args.length==1)
        {
            try
            {
                String[] argsForSmWithoutId = SpaceMarine.scanSpaceMarine();
                String[] argsForSm = new String[argsForSmWithoutId.length+1];
                argsForSm[0]=(String)args[0];
                System.arraycopy(argsForSmWithoutId, 0, argsForSm, 1, argsForSm.length - 1);

                newSm = SpaceMarine.newSpaceMarine(argsForSm);
            }catch (EndOfScanException e)
            {
                return ErrorReturn.endOfScan();
            }catch (NoSuchElementException | IllegalArgumentException | SpaceMarine.MakeSpacemarineException | Chapter.MakeChapterException e)
            {
                return new ErrorReturn(2, e.getMessage());
            }

            try {
                outputArgs.add(Long.valueOf((String) args[0]));
                outputArgs.add(newSm);
                return ErrorReturn.OK();
            }catch (NumberFormatException e)
            {
                return new ErrorReturn(2, "Id введён неверно");
            }
        }

        String[] stringArgs = new String[SpaceMarine.countOfArgumentsWithoutId+1]; //+1(id)
        int len = args.length;

        for(int i=0;i<args.length;i++)
            stringArgs[i]=(String)args[i];

        if(args.length==SpaceMarine.countOfArgumentsWithoutId-2+1 && args[len-1].equals("+"))
        {
            int lenStrArgs=stringArgs.length;
            if(listWithArgs.size()<2)
                return new ErrorReturn(2, "Мало аргументов для вызова функции");

            stringArgs[lenStrArgs-2]=listWithArgs.getFirst();
            listWithArgs.removeFirst();
            stringArgs[stringArgs.length-1]= listWithArgs.getFirst();
            listWithArgs.removeFirst();

            if(stringArgs[stringArgs.length-2].equals("&&&") || stringArgs[stringArgs.length-1].equals("&&&"))
                return new ErrorReturn(2, "Неверно введены аргументы");
        }

        try
        {
            newSm = SpaceMarine.newSpaceMarine(stringArgs);

            outputArgs.add(Long.valueOf(stringArgs[0]));
            outputArgs.add(newSm);

            return ErrorReturn.OK();
        }catch (Throwable e)
        {
            return new ErrorReturn(2, e.getMessage());
        }

    }
}
