package network;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;

public class Connection
{
    private SocketAddress socketAddress;
    private SocketChannel channel;

    ////////////////////////////////////////////////////////////////////

    public SocketAddress getSocketAddress()
    {
        return socketAddress;
    }
    public void setSocketAddress(SocketAddress socketAddress)
    {
        this.socketAddress = socketAddress;
    }
    public void setChannel() throws IOException
    {
        channel = SocketChannel.open(socketAddress);
        channel.configureBlocking(false);
    }

    public SocketChannel getChannel()
    {
        return channel;
    }
    ////////////////////////////////////////////////////////////////////

    public Connection(String IP, int PORT)
    {
        socketAddress = new InetSocketAddress(IP, PORT);
    }
    public Connection(int PORT)
    {
        socketAddress = new InetSocketAddress("localhost", PORT);
    }
    public Connection()
    {
        socketAddress = new InetSocketAddress("localhost", 7531);
    }



    public Object getData() throws IOException, ClassNotFoundException, InterruptedException
    {
        ByteBuffer buffer = ByteBuffer.allocate(100000);

        int countOfBytes;

        while(buffer.position() < 4 + 2 + 2) //+2? (May be, header)
            channel.read(buffer);


        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer.array());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        countOfBytes = objectInputStream.readInt();

        while(buffer.position() != countOfBytes)
            channel.read(buffer);

        byteArrayInputStream = new ByteArrayInputStream(buffer.array());
        objectInputStream = new ObjectInputStream(byteArrayInputStream);
        objectInputStream.readInt();
        return objectInputStream.readObject();
    }

    public void send(LinkedList<Object> outputArgs) throws IOException // В outputArgs должен быть хотя бы 1 аргумент (название команды)
    {
        if(channel == null)
            setChannel();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(outputArgs);

        byte[] outputBytes = byteArrayOutputStream.toByteArray();
        ByteBuffer buffer = ByteBuffer.wrap(outputBytes);

        while(buffer.hasRemaining())
            channel.write(buffer);
    }

    public void closeChannel() throws IOException
    {
        channel.close();
    }
}
