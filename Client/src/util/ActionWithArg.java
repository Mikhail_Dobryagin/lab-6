package util;

public interface ActionWithArg<T, R>
{
    R action(T args);
}
