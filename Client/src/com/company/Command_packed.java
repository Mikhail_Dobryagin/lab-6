package com.company;

public class Command_packed
{
    String name;
    Command commandClient;
    CommandProcessingServerReturn commandFromServer = null;
    int coutOfArgs = 0;
    int coutOfConsArgs = 0;
    boolean hasOutput = false;

    public Command_packed(String name, Command command)
    {
        this.name = name;
        this.commandClient = command;
    }

    public Command_packed(String name, Command command, Integer countOfArgs, boolean hasOutput)
    {
        this.name = name;
        this.commandClient = command;
        this.coutOfArgs = countOfArgs;
        this.hasOutput = hasOutput;
    }

    public Command_packed(String name, Command command, Integer countOfArgs, Integer countOfConsArgs, boolean hasOutput)
    {
        this.name = name;
        this.commandClient = command;
        this.coutOfArgs = countOfArgs;
        this.coutOfConsArgs = countOfConsArgs;
        this.hasOutput = hasOutput;
    }

}
