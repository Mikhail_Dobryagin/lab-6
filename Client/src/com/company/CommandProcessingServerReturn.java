package com.company;

import java.util.LinkedList;

public interface CommandProcessingServerReturn
{
    void execute(LinkedList<Object> argsFromServer);
}
