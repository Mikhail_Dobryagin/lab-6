package com.company;

import commands.server.CommandSout;
import network.Connection;
import util.*;

import java.io.*;
import java.nio.channels.NonReadableChannelException;
import java.nio.channels.NonWritableChannelException;
import java.nio.channels.NotYetConnectedException;
import java.util.*;

public class Main
{
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException
    {

        ColAnsi.init();

        LinkedList<String> list = new LinkedList<>();

        Invoker invoker = new Invoker();

        registerCommands(invoker, list);


        HashSet<String> commandsSet = new HashSet<>();

        Connection connection = null;

        try {
            if (args.length >= 2)
                connection = new Connection(args[0], Integer.parseInt(args[1]));
            if (args.length == 1)
                connection = new Connection(Integer.parseInt(args[0]));
        }catch (IllegalArgumentException | IllegalStateException e)
        {
            System.out.println(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            return;
        }

        if(args.length == 0)
            connection = new Connection();

        while(true)
        {
            if(list.isEmpty())           //Если команда вводится с консоли
            {
                commandsSet.clear();
                String[] commandWithArgs;

                try {
                    commandWithArgs = scanCommand();
                }catch (NoSuchElementException | IllegalStateException e)
                {
                    System.out.println(ColAnsi.ANSI_RED + ErrorReturn.endOfScan().getStatus() + ColAnsi.ANSI_RESET);
                    return;
                }

                String commandName = commandWithArgs[0];

                if (commandWithArgs.length == 1)
                {
                    LinkedList<Object> outputArgs = new LinkedList<>();

                    ErrorReturn error = invoker.execute(commandName, outputArgs);

                    if (error.getCode() == 1) {
                        System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        return;
                    } else if (error.getCode() == 2) {
                        if (error.getStatus() != null)
                            System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    try {
                        connection.setChannel();
                        connection.send(outputArgs);
                    }catch (IOException | NotYetConnectedException | NonWritableChannelException e)
                    {
                        System.out.println(ColAnsi.ANSI_YELLOW + "Невозможно отправить данные на сервер " + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    if(invoker.hasAnswer(commandName))
                        invoker.processAnswerFromServer(commandName, (LinkedList<Object>)connection.getData());

                    connection.closeChannel();
                }
                else
                {
                    if(invoker.getCountOfConsoleArgs(commandName)!=commandWithArgs.length-1)
                    {
                        System.out.println(ColAnsi.ANSI_YELLOW + "Аргументы введены неправильно" + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    LinkedList<Object> outputArgs = new LinkedList<>();

                    ErrorReturn error = invoker.execute(commandName, new Object[]{commandWithArgs[1]}, list, outputArgs);

                    if(error.getCode()==1)
                    {
                        System.out.println();
                        System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        return;
                    }

                    if(error.getCode()==0)
                    {
                        actionWithoutError(invoker, commandName, outputArgs, error, connection);
                        System.out.println(error.getStatus());
                    }
                    else
                        System.out.println(ColAnsi.ANSI_YELLOW+error.getStatus()+ColAnsi.ANSI_RESET);

                }

                continue;
            }

            if(list.getFirst().equals("&&&"))
            {
                list.removeFirst();
                continue;
            }

            if(list.getFirst().contains("&&&"))
            {
                System.out.println(ColAnsi.ANSI_RED+"Неверно введены аргументы"+ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            String[] commandNameWithArg = list.getFirst().split(" ", 2);
            list.removeFirst();

            String commandName = commandNameWithArg[0];

            if(!list.isEmpty() && commandName.equals("execute_script"))
            {
                if(commandsSet.contains(list.getFirst())) {
                    System.out.println(ColAnsi.ANSI_RED + "У вас нашли рекурсию\nЗдоровья погибшим" + ColAnsi.ANSI_RESET);
                    list.clear();
                    continue;
                }
                commandsSet.add(list.getFirst());
            }


            int countOfArgs = invoker.getCountOfArgs(commandName);
            int countOfConsoleArgs = invoker.getCountOfConsoleArgs(commandName);

            if(countOfArgs<0)
            {
                System.out.println(ColAnsi.ANSI_RED + "Команды " + commandName + " не существует" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            if(countOfArgs - countOfConsoleArgs > list.size())
            {
                System.out.println(ColAnsi.ANSI_RED + "Мало аргументов для вызова функции" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            ErrorReturn error=null;

            Object[] argsForCommand = new Object[countOfArgs];
            for(int i=0;i<countOfArgs;i++)
            {
                if(i==0 && commandNameWithArg.length==2)
                    argsForCommand[i]=commandNameWithArg[1];
                else {
                    argsForCommand[i] = list.getFirst();
                    list.removeFirst();
                }

                if(((String)argsForCommand[i]).contains("&&&"))
                {
                    error=new ErrorReturn(2, "Неверно введены аргументы" + "\n" + argsForCommand[i]);
                    break;
                }
            }

            if(error!=null)
            {
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            LinkedList<Object> outputArgs = new LinkedList<>();

            error = countOfArgs == 0 ? invoker.execute(commandName, outputArgs) : invoker.execute(commandName, argsForCommand, list, outputArgs);

            if(error.getCode()==1)
            {
                System.out.println();
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                return;
            }

            if(error.getCode()==0)
            {
                actionWithoutError(invoker, commandName, outputArgs, error, connection);

                continue;
            }

            System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
            list.clear();

        }

    }

    private static void actionWithoutError(Invoker invoker, String commandName, LinkedList<Object> outputArgs, ErrorReturn error, Connection connection) throws IOException, ClassNotFoundException, InterruptedException
    {

        if (error.getStatus() != null)
            System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);

        if(!commandName.equals("execute_script"))
        {
            try {
                connection.setChannel();
            }catch (IOException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно присоединиться к серверу" + ColAnsi.ANSI_RESET);
                return;
            }

            try {
                connection.send(outputArgs);
            }catch (IOException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Произошла ошибка вывода" + ColAnsi.ANSI_RESET);
                return;
            }catch (NotYetConnectedException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Канал не присоединён к серверу" + ColAnsi.ANSI_RESET);
                return;
            }catch (NonWritableChannelException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно осуществить отправку данных на сервер" + ColAnsi.ANSI_RESET);
                return;
            }

            try {
                if (invoker.hasAnswer(commandName))
                    invoker.processAnswerFromServer(commandName, (LinkedList<Object>) connection.getData());
            }catch (IOException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Произошла ошибка при чтении данных с сервера" + ColAnsi.ANSI_RESET);
                return;
            }catch (NonReadableChannelException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно получить данные с сервера" + ColAnsi.ANSI_RESET);
                return;
            }catch (NotYetConnectedException e)
            {
                System.out.println(ColAnsi.ANSI_RED + "Канал не присоединён к серверу" + ColAnsi.ANSI_RESET);
                return;
            }

            connection.closeChannel();
        }
    }


    public static String[] scanCommand()
    {
        String[] lines;
        Scanner in = new Scanner(System.in);

        lines = in.nextLine().split(" ", 2);
        return lines;
    }

    private static void registerCommands(Invoker invoker, LinkedList<String> list)
    {
        invoker.register("help", new commands.client.CommandHelp());
        invoker.register("info", new commands.client.CommandInfo());
        invoker.register("show", new commands.client.CommandShow());
        invoker.register("add", new commands.client.CommandAdd(), 8, true);
        invoker.register("add_if_min", new commands.client.CommandAdd(), 8, true);
        invoker.register("update", new commands.client.CommandUpdate(), 9, 1, true);
        invoker.register("remove_by_id", new commands.client.CommandRemoveById(), 1, 1, true);
        invoker.register("clear", new commands.client.CommandClear());
        invoker.register("execute_script", new commands.client.CommandExecuteScript(list), 1, 1);
        invoker.register("exit", new commands.client.CommandExit());
        invoker.register("remove_first", new commands.client.CommandRemoveFirst());
        invoker.register("remove_head", new commands.client.CommandRemoveHead());
        invoker.register("filter_contains_name", new commands.client.CommandFilterContainsName(), 1, 1, true);
        invoker.register("filter_starts_with_name",new commands.client.CommandFilterStartsWithName(), 1, 1, true);
        invoker.register("print_descending", new commands.client.CommandPrintDescending());

        invoker.registerSecondCommand("help", new CommandSout());
        invoker.registerSecondCommand("info", new CommandSout());
        invoker.registerSecondCommand("show", new commands.server.CommandShow());
        invoker.registerSecondCommand("add", new commands.server.CommandSout());
        invoker.registerSecondCommand("add_if_min", new commands.server.CommandSout());
        invoker.registerSecondCommand("update", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_by_id", new commands.server.CommandSout());
        invoker.registerSecondCommand("clear", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_first", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_head", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_contains_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_starts_with_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("print_descending", new commands.server.CommandShow());
    }
}
