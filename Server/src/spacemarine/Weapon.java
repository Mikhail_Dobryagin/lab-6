package spacemarine;

import java.io.Serializable;

/**
 * Оружие
 */
public enum Weapon implements Serializable
{
    BOLTGUN,
    BOLT_RIFLE,
    PLASMA_GUN,
    COMBI_PLASMA_GUN,
    INFERNO_PISTOL;

    public static final long serialVersionUID = 6L;
}
