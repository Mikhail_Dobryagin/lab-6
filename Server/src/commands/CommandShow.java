package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandShow implements Command
{
    private final PQofSpacemarines pqs;
    public CommandShow(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return pqs.show();
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }

}
