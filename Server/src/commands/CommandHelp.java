package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandHelp implements Command
{

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        outputArgs.add(PQofSpacemarines.help().toString());
        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }
}
