package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandRemoveById implements Command
{
    private final PQofSpacemarines pqs;
    public CommandRemoveById(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        try {
            outputArgs.add(pqs.remove_by_id((Long)args.poll()) ? "Элемент успешно удалён" : "Элемента с таким id не существует");
        }catch (NumberFormatException e)
        {
            outputArgs.add("Ошибка при вводе Id");
        }

        return outputArgs;
    }
}
