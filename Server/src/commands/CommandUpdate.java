package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import spacemarine.SpaceMarine;

import java.util.LinkedList;

public class CommandUpdate implements Command
{
    private final PQofSpacemarines pqs;

    public CommandUpdate(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {

        Long id = (Long)args.poll();
        SpaceMarine newSm = (SpaceMarine)args.poll();

        LinkedList<Object> outputArgs = new LinkedList<>();

        outputArgs.add(pqs.update(id, newSm) ? "Элемент успешно обновлён" : "Элемент с таким id не найден");

        return outputArgs;
    }
}
