package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandFilterContainsName implements Command
{
    private final PQofSpacemarines pqs;

    public CommandFilterContainsName(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {

        return pqs.filter_contains_name((String)args.poll());
    }
}
