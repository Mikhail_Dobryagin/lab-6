package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandRemoveFirst implements Command
{
    private final PQofSpacemarines pqs;

    public CommandRemoveFirst(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        outputArgs.add(pqs.remove_first() ? "Элемент успешно удалён" : "Очередь пуста");

        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }
}
