package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandClear implements Command
{
    private final PQofSpacemarines pqs;
    public CommandClear(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> outputArgs = new LinkedList<>();
        pqs.clear();
        outputArgs.add("Коллекция успешно очищена");
        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }

}
