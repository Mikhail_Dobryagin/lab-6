package commands;

import com.company.Command;

import com.company.PQofSpacemarines;

import java.util.LinkedList;
import java.util.PriorityQueue;

public class CommandFilterStartsWithName implements Command
{
    private final PQofSpacemarines pqs;

    public CommandFilterStartsWithName(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return pqs.filter_starts_with_name((String)args.poll());
    }
}
