package commands;

import com.company.Command;
import spacemarine.Chapter;
import com.company.PQofSpacemarines;
import spacemarine.SpaceMarine;

import java.util.LinkedList;
import java.util.PriorityQueue;


public class CommandAdd implements Command
{
    private final PQofSpacemarines pqs;
    public CommandAdd(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        SpaceMarine sm = (SpaceMarine)args.poll();
        sm.setId(pqs.getCommonId());

        try{
            outputArgs.add(pqs.add(sm) ? "Вставка прошла успешно" : "Не удалось вставить элемент");
        }catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException e)
        {
            outputArgs.add(e.getMessage());
        }

        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }
}

