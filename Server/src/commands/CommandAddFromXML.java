package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;
import util.FileException;

import javax.xml.parsers.ParserConfigurationException;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class CommandAddFromXML implements Command
{
    private final PQofSpacemarines pqs;

    public CommandAddFromXML(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> result = new LinkedList<>();

        try
        {
            pqs.addFromXML();
            result.add(new ErrorReturn(0));
        }catch (ParserConfigurationException | IllegalArgumentException | NullPointerException | FileException e)
        {
            result.add(new ErrorReturn(2, e.getMessage()));
        }

        return result;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }

}
