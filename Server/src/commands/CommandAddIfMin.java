package commands;

import com.company.Command;
import spacemarine.Chapter;
import com.company.PQofSpacemarines;
import spacemarine.SpaceMarine;

import java.util.LinkedList;

public class CommandAddIfMin implements Command
{
    private final PQofSpacemarines pqs;

    public CommandAddIfMin(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        SpaceMarine sm = (SpaceMarine)args.poll();
        sm.setId(pqs.getCommonId());

        try{
            outputArgs.add(pqs.add_if_min(sm) ? "Вставка прошла успешно" : "Не удалось вставить элемент");
            return outputArgs;
        }catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException e)
        {
            outputArgs.add(e.getMessage());
            return outputArgs;
        }
    }

    @Override
    public LinkedList<Object> execute()
    {
        return null;
    }
}
