package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandPrintDescending implements Command
{
    private final PQofSpacemarines pqs;

    public CommandPrintDescending(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        return pqs.print_descending();
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }
}
