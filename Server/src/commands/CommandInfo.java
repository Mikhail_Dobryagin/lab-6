package commands;

import com.company.Command;
import com.company.PQofSpacemarines;

import java.util.LinkedList;
import java.util.PriorityQueue;

public class CommandInfo implements Command
{
    private final PQofSpacemarines pqs;
    public CommandInfo(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> outputArgs = new LinkedList<>();
        outputArgs.add(pqs.info().toString());
        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }

}
