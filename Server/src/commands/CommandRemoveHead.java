package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandRemoveHead implements Command
{
    private final PQofSpacemarines pqs;

    public CommandRemoveHead(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public LinkedList<Object> execute()
    {
        LinkedList<Object> outputArgs = new LinkedList<>();

        Object sm = pqs.remove_head();


        outputArgs.add(sm == null ? new ErrorReturn(2, "Очередь пуста") : ErrorReturn.OK());
        if(sm!=null)
            outputArgs.add(sm);

        return outputArgs;
    }

    @Override
    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        return null;
    }

}
