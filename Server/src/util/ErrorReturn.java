package util;

import java.io.Serializable;

/**
 * Код возврата и описание выполнения методов
 * 0 -- ОК
 * 1 -- Всё плохо (рекомендуется завершить программу)
 * 2 -- Произошла ошибка (Можно продолжить программу)
 * String -- описание возвращаемого значения
 */
public class ErrorReturn implements Serializable
{
    private final Pair<Integer, String> error;
    public static final long serialVersionUID = 8L;


    /**
     * @param value Код возврата
     * @param errorS Описание возвращаемого значения
     */
    public ErrorReturn(int value, String errorS)
    {
        error = new Pair<>(value, errorS);
    }

    /**
     * Описание возвращаемого значения отсутствует
     * @param value Код возврата
     */
    public ErrorReturn(int value)
    {
        error = new Pair<>(value, null);
    }

    /**
     * Получить код возврата
     */
    public int getCode()
    {
        return error.first;
    }

    /**
     * Ошибка при блокировании ввода
     */
    public static ErrorReturn endOfScan()
    {
        return new ErrorReturn(1, "Не удалось прочитать строку из-за некоректного ввода");
    }

    /**
     * Отсутствие какой-либо ошибки
     */
    public static ErrorReturn OK()
    {
        return new ErrorReturn(0);
    }

    /**
     * Получить описание возвращаемого значения
     */
    public String getStatus()
    {
        return error.second;
    }
}
