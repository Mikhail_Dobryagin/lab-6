package com.company;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Класс, управляющий выполнением комманд
 */
public class Invoker
{
    /**
     * Словарь из комманд -- <имя комманды, <объект комманды, <количество аргументов <i>общее(может быть больше -- тогда в команду нужно передавать список команд)</i>, <количество аргументов <i>для консоли</i>>>>>
     */
    private final HashMap<String, Command> commandMap= new HashMap<>();

    /**
     * Добавить команду <b>без аргументов</b>
     * @param commandName Название команды
     * @param command Объект комманды
     */
    public void register(String commandName, Command command) {
        commandMap.put(commandName, command);
    }

    public LinkedList<Object> execute(LinkedList<Object> args)
    {
        String commandName = (String)args.poll();

        return args.isEmpty() ? commandMap.get(commandName).execute() : commandMap.get(commandName).execute(args);

    }
    /**
     * Проверить, добавлена ли указанная команда
     * @param commandName Название команды
     */
    public boolean isInInvoker(String commandName)
    {
        return  !(commandMap.get(commandName)==null);
    }

}
