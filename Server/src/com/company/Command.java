package com.company;


import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Интерфейс обобщающий различные команды
 */
public interface Command
{
    /**
     * Выполнить команду, не указывая аргументы
     * @return ErrorReturn<int,String><br/>int-- {0 - OK; 1 - Всё плохо (рекомендуется завершить программу); 2 - Произошла ошибка (Можно продолжить программу)}, String -- описание
     */
    LinkedList<Object> execute();
    /**
     * Выполнить команду с указанием ургументов
     * @return ErrorReturn<int,String><br/>int-- {0 - OK; 1 - Всё плохо (рекомендуется завершить программу); 2 - Произошла ошибка (Можно продолжить программу)}, String -- описание
     */
    LinkedList<Object> execute(LinkedList<Object> args);

}
