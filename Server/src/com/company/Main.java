package com.company;
import commands.*;
import network.Connection;
import util.ColAnsi;
import util.ErrorReturn;
import util.FileException;
import util.Pair;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main
{

    static Logger logger;
    static {
        try (FileInputStream ins = new FileInputStream("./log_config.txt")) {
            LogManager.getLogManager().readConfiguration(ins);
            logger = Logger.getLogger(Main.class.getName());
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException
    {

        logger.log(Level.INFO, "Запуск сервера");

        ColAnsi.init();

        PQofSpacemarines pqs = new PQofSpacemarines();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                Thread.sleep(200);
                System.out.println(ColAnsi.ANSI_GREEN + "Пока - пока ☺" + ColAnsi.ANSI_RESET);

                try
                {
                    pqs.save();
                    logger.log(Level.INFO, "Сохранение коллекции...");
                }catch (FileException e)
                {
                    logger.log(Level.SEVERE, ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
                }

                logger.log(Level.INFO, "Отключение сервера");

            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            }
        }));

        {
            ErrorReturn error = (ErrorReturn)(new CommandAddFromXML(pqs)).execute().poll();
            if(error.getCode()!=0)
                logger.log(Level.WARNING, ColAnsi.ANSI_RED+error.getStatus()+ColAnsi.ANSI_RESET);
            else
                logger.log(Level.INFO, ColAnsi.ANSI_GREEN + "Коллекция успешно добавлена из файла" + ColAnsi.ANSI_RESET);
        }

        Connection connection = null;

        if(args.length != 0)
        {
            logger.log(Level.INFO, "Задан порт " + args[0]);
            try{
                connection = new Connection(Integer.parseInt(args[0]));
            }catch (IllegalStateException | IllegalArgumentException e)
            {
                logger.log(Level.SEVERE, ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
                return;
            }
        }
        else
            connection = new Connection();

        logger.log(Level.INFO, "Подключение сервера к порту " + connection.getPORT());

        Invoker invoker = new Invoker();
        registerCommands(invoker, pqs);


        while(true)
        {
            if(!connection.getNewClient())
            {
                int commandFromTerminal = scanTerminal();
                if (commandFromTerminal == 1) return;
                if(commandFromTerminal == 0) pqs.save();
                continue;
            }


            logger.log(Level.INFO, "Получение данных от клиента...");
            Pair<ErrorReturn, LinkedList<Object>> errorWithReturn = connection.input(); //commandName + args(may be not)

            LinkedList<Object> argsForCommand = errorWithReturn.second;

            LinkedList<Object> outputArgs = new LinkedList<>();

            if(errorWithReturn.first.getCode() != 0)
            {
                outputArgs.add(errorWithReturn.first);
                logger.log(Level.WARNING, errorWithReturn.first.getStatus());
                connection.output(outputArgs);
                logger.log(Level.INFO, "Отправка данных об ошибке клиенту");
                continue;
            }

            logger.log(Level.INFO, "Исполнение команды...");
            outputArgs = invoker.execute(argsForCommand);

            logger.log(Level.INFO, "Отправка данных клиенту");
            connection.output(outputArgs);

            int commandFromTerminal = scanTerminal();
            if (commandFromTerminal == 1) return;
            if(commandFromTerminal == 0) pqs.save();
        }
    }

    private static int scanTerminal() throws IOException
    {
        if(System.in.available()!=0)
        {
            Scanner scanner = new Scanner(System.in);
            String commandFromConsole;

            try {
                commandFromConsole = scanner.nextLine();
                logger.log(Level.INFO, "Введена команда " + commandFromConsole);
            }catch (NoSuchElementException | IllegalStateException e)
            {
                logger.log(Level.SEVERE,ColAnsi.ANSI_RED + "Произошло закрытие ввода" + ColAnsi.ANSI_RESET);
                return -1;
            }

            return commandFromConsole.equals("save") ? 0 : commandFromConsole.equals("exit") ? 1 : -1;

        }

        return -1;
    }

    private static void registerCommands(Invoker invoker, PQofSpacemarines pqs)
    {
        invoker.register("help", new CommandHelp());
        invoker.register("info", new CommandInfo(pqs));
        invoker.register("show", new CommandShow(pqs));
        invoker.register("add", new CommandAdd(pqs));
        invoker.register("update", new CommandUpdate(pqs));
        invoker.register("remove_by_id", new CommandRemoveById(pqs));
        invoker.register("clear", new CommandClear(pqs));
        invoker.register("remove_first", new CommandRemoveFirst(pqs));
        invoker.register("remove_head", new CommandRemoveHead(pqs));
        invoker.register("add_if_min", new CommandAddIfMin(pqs));
        invoker.register("filter_contains_name", new CommandFilterContainsName(pqs));
        invoker.register("filter_starts_with_name", new CommandFilterStartsWithName(pqs));
        invoker.register("print_descending", new CommandPrintDescending(pqs));

    }
}
