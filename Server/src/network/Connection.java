package network;

import util.ErrorReturn;
import util.Pair;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;


public class Connection
{
    private int PORT = 7531;
    private ServerSocket serverSocket;
    private Socket socket;

    /////////////////////////////////////////////////////////////////////////////////

    public int getPORT()
    {
        return PORT;
    }
    public void setPORT(int PORT)
    {
        this.PORT = PORT;
    }
    public ServerSocket getServerSocket()
    {
        return serverSocket;
    }
    public void setServerSocket(ServerSocket serverSocket)
    {
        this.serverSocket = serverSocket;
    }
    public Socket getSocket()
    {
        return socket;
    }
    public void setSocket(Socket socket)
    {
        this.socket = socket;
    }

    /////////////////////////////////////////////////////////////////////////////////
    public Connection(int PORT) throws IOException
    {
        this.PORT = PORT;
        serverSocket = getServer(this.PORT);
    }

    public Connection() throws IOException
    {
        serverSocket = getServer(this.PORT);
    }

    private static ServerSocket getServer(int PORT) throws IOException
    {
        return new ServerSocket(PORT);
    }

    public boolean getNewClient()
    {
        try {
            serverSocket.setSoTimeout(100);
            socket = serverSocket.accept();
            return true;
        } catch (IOException e)
        {
            return false;
        }
    }

    @SuppressWarnings({"unchecked"})
    public Pair<ErrorReturn, LinkedList<Object>> input() throws IOException
    {
        try
        {
            InputStream inputStreamFromSocket  = socket.getInputStream();
            ObjectInputStream inputStream = new ObjectInputStream(inputStreamFromSocket);
            Object in = inputStream.readObject();

            LinkedList<Object> outputArgs = (LinkedList<Object>)in;

            return outputArgs == null ? new Pair<>(new ErrorReturn(2, "Произошла ошибка"), new LinkedList<>()) :
                    new Pair<>(ErrorReturn.OK(), outputArgs);
        } catch (ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
            return new Pair<>(new ErrorReturn(2, e.getMessage()), new LinkedList<>());
        }


    }

    public <T> ErrorReturn output(T args)
    {
        try
        {
            ByteArrayOutputStream internalByteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream internalObjectOS = new ObjectOutputStream(internalByteArrayOutputStream);
            internalObjectOS.writeObject(args);


            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeInt(4 + internalByteArrayOutputStream.size() + 2); //+2? (May be, header)
            outputStream.writeObject(args);

            return ErrorReturn.OK();
        } catch (ClassCastException | IOException e) {
            e.printStackTrace();
            return new ErrorReturn(2, "Ошибка при попытке ответа клиенту");
        }
    }

}
